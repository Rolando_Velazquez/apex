import pymysql
import sys
from app.Modelo.conexion import conexion

class Configuration:

  def __init__(self):
    self.EConexion = conexion()
    self.conexion = self.EConexion.getConexion()

  def modificar(self,Email,User,Password,Id):
    try:
      with self.conexion.cursor() as Q1:
        sql = 'call SP_CUD_User(2,%s,%s,%s,%s);'
        Q1.execute(sql,(Email,User,Password,Id))
        self.conexion.commit()
        Q1.close()
    except:
      print("El error es", sys.exc_info())

  def eliminar(self,Id):
    try:
      with self.conexion.cursor() as Q2:
        sql = 'call SP_CUD_User(1,null,null,null,%s)'
        Q2.execute(sql,(Id))
        self.conexion.commit()
        Q2.close()
    except:
      print("El error es:", sys.exc_info())

  def CrearGrupo(self,NombreGrupo,Codigo,Id):
    try:
      with self.conexion.cursor() as Q3:
        sql = 'call SP_CUDGroup(1,%s,%s,%s,null)'
        Q3.execute(sql,(NombreGrupo,Codigo,Id))
        self.conexion.commit()
        Q3.close()
    except:
      print("El error es: ",sys.exc_info())
  
  def EditarGrupo(self,NombreGrupo,IdGrupo):
    try:
      with self.conexion.cursor() as Q4:
        sql = 'call SP_CUDGroup(2,%s,null, null,%s)'
        Q4.execute(sql,(NombreGrupo,IdGrupo))
        self.conexion.commit()
        Q4.close()
    except:
      print("Error en :",sys.exc_info())

  def EliminarGrup(self,IdGrupo):
    try:
      with self.conexion.cursor() as Q5:
        sql = 'call SP_CUDGroup(3,null,null, null,%s)'
        Q5.execute(sql,(IdGrupo))
        self.conexion.commit()
        Q5.close()
    except:
      print("Error en :", sys.exc_info())

class OGrupos:
  def __init__(self):
    self.EConexion = conexion()
    self.conexion = self.EConexion.getConexion()
  
  def ObtenerGrupo(self,Id):
    try:
      with self.conexion.cursor() as Q1:
        sql = 'call SP_ShowGroup(%s)'
        Q1.execute(sql,(Id))
        o = Q1.fetchall()
        print(o)
      return o
    except:
      print("El error es: ",sys.exc_info())

  def ObtenerCodigo(self,idgrupo):
    try:
      with self.conexion.cursor() as Q2:
        sql = 'call findKeey(%s)'
        Q2.execute(sql,(idgrupo))
        co = Q2.fetchone()
        print("xodio",co)
      return co
    except:
      print("El error esta en ",sys.exc_info())
