from tkinter import *
from tkinter import font
from app.Controlador.controlchat.configuracion.configuracion import configuracion

class InterfazHija:

  def __init__(self,vista,IdUsuario,control):
    self.ID = IdUsuario
    print("Id usuario ",self.ID)
    self.control = control
    self.vista = vista
    self.conf=Toplevel(self.vista, bg="#F2F2F2")
    self.conf.geometry("280x480+500+170")
    self.emailUser = StringVar()
    self.userName = StringVar()
    self.contrUser=StringVar()
    self.ima = PhotoImage (file = "src/perfil.png")
    self.conf.focus_set()
    self.conf.grab_set()
    self.conf.transient(master=vista)
    self.configuracionn = configuracion()

  def Iniciar(self):
    Helvfont = font.Font(family="Helvetica", size = 14)
    Hel = font.Font(family="Arial", size = 14)

    Label (self.conf, image=self.ima,relief= FLAT, bg="#F2F2F2").place(x="95",y="15")
    Label (self.conf, text = "Correo electronico", bg = "#F2F2F2", font = Helvfont).place(x= 65, y= 125)
    Entry (self.conf, relief = SOLID, font=Hel).place(x=40,y=160, width= 215, height=30)

    Label (self.conf, text = "Nombre de usuario", bg = "#F2F2F2", font = Helvfont).place(x= 65, y= 220) 	
    Entry (self.conf, relief = SOLID, font=Hel).place (x=40, y = 255,width = 215, height=30)

    Label (self.conf, text = "Contraseña", bg = "#F2F2F2", font = Helvfont).place(x= 85, y= 320)
    Entry(self.conf, textvariable=self.contrUser,relief = SOLID, font = Hel).place (x= 40, y=355, width=215, height=30)

    b1=Button (self.conf, text = "Eliminar cuenta", font= Hel,cursor = "hand2", fg= "#0F1419", relief = FLAT)
    b1.bind("<Button-1>",self.eliminar) 
    b1.place (x=145, y=410, width = 130, height=35)

    b1=Button (self.conf, text = "Enviar", font= Hel,cursor = "hand2", fg= "#0F1419", relief = FLAT)
    b1.bind("<Button-1>",self.modificar) 
    b1.place (x=5,y=410, width = 100, height=35)

  def cerrar(self,event):

    self.conf.destroy()
  
  def modificar(self,event): 
    
    self.configuracionn.modificar(self.emailUser.get(),self.userName.get(),self.contrUser.get(),int(self.ID))
    print(self.emailUser.get())
    self.conf.destroy()
    self.vista.destroy()
    self.control.setAccion("Chat",int(self.ID))


  def eliminar(self, event):
    self.configuracionn.eliminar(self.ID)
    self.vista.destroy()
    self.control.setAccion("Index",int(self.ID))
    self.conf.destroy()