from tkinter import *
from tkinter import font

class IngresarCodigo():

    def __init__(self):

        self.Codigo = Tk()
        self.Codigo.resizable(0,0)
        self.Codigo.overrideredirect(True)
        self.Codigo.geometry("420x300+482+250")
        self.Codigo.title("Enter your code")

        self.frameco = Frame()
        self.frameco.config(bg="#F4B183")
        self.frameco.place(x=0, y=0)
        self.frameco.config(width="420",height="300")
        
        Ari = font.Font(family="Arial", size = 14)
        Cur = font.Font(family="Bell MT", size= 18)

        Entry(self.Codigo, font = Ari, bg="#F2F2F2", relief=FLAT).place(x=100, y=90, width =230, height = 30)
        Label(self.Codigo, text="Enter your code", font = Cur, fg="black", bg="#F4B183").place(x=135, y=40)
        Label (self.Codigo,text = "Confirm Password", font = Cur, bg = "#F4B183"). place (x=120, y = 150)
       

        Button(self.Codigo, text="Entry", font=Cur, bg="#EE8944", relief=FLAT).place(x=340, y=252.5)
        Entry (self.Codigo,bg="#F2F2F2", relief = FLAT).place(x=100, y = 190, width = 230, height = 30)


       
        b = Button(self.Codigo, text="Cancel", bg="#EE8944", relief=FLAT, font = Cur)
        b.bind("<Button-1>", self.cerrar)
        b.place(x=0, y=252.5)

        self.Codigo.mainloop()

    def cerrar(self,event):
        self.Codigo.destroy()

c = IngresarCodigo()