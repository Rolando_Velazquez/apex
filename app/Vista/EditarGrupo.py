from tkinter import*
from tkinter import font

from app.Modelo.modeloconfig.configutation import Configuration

class EditarGrupo:
    def __init__(self,vista,usuario,IdGrupo,VChat):
      self.IdGrupo = IdGrupo
      self.IdUsuario = usuario
      self.VChat = VChat
      print(self.IdGrupo)
      print(self.IdUsuario)
      self.vista= vista
      self.eg = Toplevel(self.vista,bg ="#F4B183")
      self.eg.resizable(0,0)
      self.eg.geometry("400x200+482+250")
      self.eg.focus_set()
      self.eg.grab_set()
      self.eg.transient(master=self.vista)
      self.NombreGrupo = StringVar()
      
    def Iniciar(self):
      Helvfont = font.Font(family="Helvetica", size = 18)
      Hel = font.Font(family="Arial" ,size=13)

      Label (self.eg, bg ="#F4B183",text="Nuevo nombre del grupo:", font= Helvfont).place(x= 75, y =40)
      Entry (self.eg, textvariable= self.NombreGrupo, font= Hel).place(x=110, y = 100, width=220, height=30)

      B1 = Button (self.eg, text = "OK", font = Hel)
      B1.bind("<Button-1>",self.Editar)
      B1.place (x=170, y = 160, width=80, height=30)
 
    def Editar(self,event):
      conf = Configuration()
      conf.EditarGrupo(self.NombreGrupo.get(),self.IdGrupo)
      self.eg.destroy()
      self.VChat.sett()
