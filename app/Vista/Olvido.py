from tkinter import*
from tkinter import font

class Olvidar():

    def __init__(self):
        self.o = Tk ()
        self.o.resizable(0,0)
    
    def Iniciar(self,accion):
        self.accion = accion
        self.o.title("¿Ha olvidado su contraseña?")
        self.o.overrideredirect(True)
        self.o.geometry("400x200+482+250")

        self.frame=Frame()
        self.frame.place(x=0)
        self.frame.config(bg="#F2A36E") 
        self.frame.config(width="400",height="200")

        Helvfont = font.Font(family="Helvetica", size = 20)
        Hel = font.Font(family="Arial", size = 15)

        Label (self.o, text="Lo sentimos,\n sección no disponible",font = Helvfont, fg ="#FFF5EE", bg="#F2A36E").place(x=60,y=50)
        b1 = Button (self.o, text="Ok", font = Hel,borderwidth =5, fg="#F2A36E",bd = 5 ,relief = FLAT, cursor = "hand2")
        b1.bind("<Button-1>",self.cerrar)
        b1.place(x=140,y = 140, width=120, height=35)

        self.o.mainloop()
    def cerrar(self,event):
        self.o.destroy()
        self.accion.setAccion("Index")