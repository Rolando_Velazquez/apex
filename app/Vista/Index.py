from tkinter import font
from tkinter import * 
#from app.Controlador.ControlVistas.ejec import ControlVistas


class InterfazIndex():

	def __init__(self):
		
		self.InterfazIndex = Tk()
		self.InterfazIndex.resizable(0,0)
		self.InterfazIndex.config(bg="white")
		self.InterfazIndex.geometry("350x500+500+125")
		self.InterfazIndex.iconbitmap("src/f.ico")
		self.InterfazIndex.title("APPX | Index")

	def Iniciar(self,control):
		
		self.control = control
		Helvfont = font.Font(family="Helvetica", size = 24)
		Hel = font.Font(family="Arial", size = 20)
		Helv = font.Font(family="Arial", size = 15)

		Label(self.InterfazIndex, text = "Welcome to ", border=0, relief="solid", font = Helvfont, fg="#4d4d4d", bg="white").place(x = 100, y = 20)
		Label(self.InterfazIndex, text = "APPX ", border=0, relief="solid", font = Helvfont, fg="#4d4d4d", bg="white").place(x = 135, y = 50)
		Label(self.InterfazIndex, text = "or", font = Hel, fg="#4d4d4d", bg="white").place(x = 150, y = 268)

		b = Button(self.InterfazIndex, text = "Log In", borderwidth=5, bg="#F4B183",font = Helv,border=0, relief="solid",cursor="hand2")
		b.bind("<Button-1>",self.A1)
		b.place(x = 80, y = 162, width=190, height=55)

		b2 = Button(self.InterfazIndex, text = "Sign Up", borderwidth=5, bg="#F4B183",font = Helv,border=0, relief="solid",cursor="hand2")
		b2.bind("<Button-1>", self.A2)
		b2.place(x = 80, y = 366, width=190, height=55)
		
		self.InterfazIndex.mainloop()

	def A1(self,event):
		self.InterfazIndex.destroy()
		self.control.setAccion("Ingresar")

	def A2(self, event):
		self.InterfazIndex.destroy()
		self.control.setAccion("Registro")


	