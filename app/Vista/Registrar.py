from app.Controlador.controlLoguin.RegistroLoguin import registroLguin
from app.Controlador.controlLoguin.ControlLoguin import controladorLoguin
from tkinter import *
from tkinter import font

class InterfazRegistro():

	def __init__(self):

		self.Registro = Tk()
		self.Registro.resizable(0,0)
		self.Registro.geometry("300x450+525+125")
		self.Registro.config(bg="white")
		self.Registro.iconbitmap("src/f.ico")
		self.Registro.title("Registrarse")
		self.Nombre = StringVar()
		self.Correo = StringVar()
		self.Contrasena = StringVar()
		
	def Iniciar(self,control):

		self.contro = control

		Helvfont = font.Font(family="Helvetica", size = 23)
		Hel = font.Font(family="Arial", size = 12)

		Label(self.Registro, text = "Crear cuenta", fg="#4d4d4d", bg="white",font = Helvfont).pack()
		Entry(self.Registro, textvariable=self.Nombre,border =1,state="normal").place(x=60, y=107, width=150, height=20)
		Entry(self.Registro, textvariable=self.Correo,border =1).place(x=60, y=177, width=150, height=20)
		Entry(self.Registro, textvariable=self.Contrasena,border =1, show="*").place(x=60, y=247, width=150, height=20)

		Label(self.Registro, text = 'User', fg="#4d4d4d", bg="white", font = Hel).place(x=60, y = 80)
		Label(self.Registro, text = 'Email', fg="#4d4d4d", bg="white", font = Hel).place(x=60, y = 150)
		Label(self.Registro, text = 'Password', fg="#4d4d4d", bg="white", font = Hel).place(x=60, y = 220)
		b1 = Button(self.Registro, text = "Registrarse", borderwidth=5, bg="#F4B183",font = Hel,border=0, relief="solid", cursor="hand2")
		b1.bind("<Button-1>",self.registrar)
		b1.place(x=110, y=360)

		c= Button(self.Registro, text="Atras", bg="#F4B183", font = Hel, border=0, relief="flat", cursor="hand2")
		c.bind("<Button-1>", self.Atra)
		c.place(x=0,y=425, width=150)

		self.Registro.mainloop()

	def registrar(self,event):
		conLo = controladorLoguin()
		registro = registroLguin(self.Nombre.get(),self.Correo.get(),self.Contrasena.get())
		registro.Insertar()
		print(self.Nombre.get(), self.Contrasena.get())
		conLo.Identificar(self.Nombre.get(),self.Contrasena.get())
		self.Registro.destroy()	
		self.contro.setAccion("Registrado",conLo.getId())


	def Atra(self,event):
		self.Registro.destroy()
		self.contro.setAccion("Index")