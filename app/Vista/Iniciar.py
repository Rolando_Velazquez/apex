from tkinter import *
from tkinter import font
from tkinter import messagebox
# from app.Controlador.accesos.verificador import Verificador
from app.Controlador.controlLoguin.ControlLoguin import controladorLoguin


class InterfazIniciar:

	def __init__(self):
		self.c = controladorLoguin()

		self.InterfazIniciar = Tk()
		self.InterfazIniciar.resizable(0,0)
		self.InterfazIniciar.geometry("250x340+550+220")
		self.InterfazIniciar.config(bg="white")
		self.InterfazIniciar.iconbitmap("src/f.ico")
		self.InterfazIniciar.title("Iniciar Sesión")
		self.Nombre = StringVar()
		self.Contrasena = StringVar()

	def Iniciar(self,control):

		self.con = control
		Helvfont = font.Font(family="Helvetica", size = 18)
		Hel = font.Font(family="Arial", size = 11)

		Label(self.InterfazIniciar, text = "Log In", fg="#4d4d4d",bg="white", font = Helvfont).place(x=90, y=10)
		Label(self.InterfazIniciar, text = 'User ',fg="#4d4d4d",bg="white", font = Hel).place(x=50, y=80)
		Label(self.InterfazIniciar, text = 'Password', fg="#4d4d4d", bg="white",font = Hel).place(x=50, y = 170)
		b1= Button(self.InterfazIniciar, text = "Forgot your password?", fg="#4d4d4d", bg="white",font = Hel, relief=FLAT)
		b1.bind("<Button-1>",self.Olvidar)
		b1.place(x= 47, y=310 )

		self.Nom =Entry(self.InterfazIniciar, textvariable=self.Nombre, border =1)
		self.Nom.place(x=50, y=102, width=150, height=20)
		Entry(self.InterfazIniciar,  textvariable=self.Contrasena,border =1, show="*").place(x=50, y=192, width=150, height=20)

		b = Button(self.InterfazIniciar, text = "Start", bg="#F4B183", font = Hel, border=0, relief="solid", cursor="hand2")
		b.bind("<Button-1>", self.okey)
		b.place(x=180, y=260)

		c = Button(self.InterfazIniciar, text="Back", bg="#F4B183", font = Hel, border=0, relief="solid", cursor="hand2")
		c.bind("<Button-1>",self.Atras)
		c.place(x=30,y=260)
		# b.bind('<Button-1>' )
		self.InterfazIniciar.mainloop()

	def Atras(self,event):
		self.InterfazIniciar.destroy()
		self.con.setAccion("Index")
	def Olvidar(self,event):
		self.InterfazIniciar.destroy()
		self.con.setAccion("Olvidar")
##Bloque para llamar 
	def okey(self, event):
		if self.c.Identificar(self.Nombre.get(), self.Contrasena.get()):
			print("Hola")
			self.InterfazIniciar.destroy()
			self.con.setAccion("Chat",self.c.getId())
		else:
			messagebox.showerror(title= "Error",message="Porfavor verifique sus credenciales")
			self.Nombre.set("")
			self.Contrasena.set("")
			self.Nom.focus_set()
			