from tkinter import *
from tkinter import font
from app.Modelo.modeloconfig.configutation import Configuration
# from app.Modelo.modeloconfig.configutation import Configuration

class EliminarGrupo:
  def __init__(self,vista,usuario,IdGrupo,VChat,eliminar):
    self.IdGrupo = IdGrupo
    self.VChat = VChat
    self.vista = vista
    self.eliminar = eliminar
    self.elg = Toplevel(self.vista, bg ="#F4B183")
    # self.gc = Toplevel(self.vista,bg ="#F4B183")
    self.elg.resizable(0,0)
    self.elg.geometry("400x200+482+250")
    self.elg.focus_set()
    self.elg.grab_set()
    self.elg.transient(master=self.vista)
  
  def Iniciar(self):
    Helvfont = font.Font(family="Helvetica", size = 18)
    Label( self.elg, text = "¿Esta seguro que quieres eliminar \n el grupo?", bg ="#F4B183", font = Helvfont).place(x= 10, y= 40)
    self.BS = Button (self.elg, text = "Si", cursor = "hand2", bg="#F2A36E")
    self.BS.bind("<Button-1>",self.Si)
    self.BS. place (x= 240, y = 140, width=140, height= 30)

    self.BE = Button (self.elg, text = "No", cursor = "hand2", bg="#F2F2F2")
    self.BE.bind("<Button-1>",self.No)
    self.BE.place (x= 40, y = 140, width=140, height= 30)

  def Si(self,event):
    conf = Configuration()
    self.elg.destroy()
    conf.EliminarGrup(self.IdGrupo)
    self.VChat.confIBoton()

  def No(self,event):
    self.elg.destroy()

