from tkinter import *
from tkinter import font

from app.Controlador.controlchat.Usuarios import MisGrupos

from app.Controlador.controlchat.setGrupos import setGrupos

from app.Vista.VHConfiguracion import InterfazHija
from app.Vista.CrearGrupo import CrearGrupo


class Chat:

	def __init__ (self):

		self.raiz= Tk()
		self.raiz.overrideredirect(False)
		self.raiz.title("Interfaz Usuario")
		self.raiz.geometry ("900x510+225+130")
		self.raiz.config(bg = "#F2A36E")
		self.Ngrupo = StringVar()
		self.codigo = StringVar()
		self.botonClick=StringVar()
	def Salir(self):
		exit()
	
	def Iniciar(self, control,Usuario):
		self.usuario = Usuario
		# print("El usuario es ",self.usuario)
		##El parametro Usuario solamente retorna un id

		self.control = control
		self.framesend=Frame()
		self.framesend.place(x=270, y=466)
		self.framesend.config(bg="#F4B183")
		self.framesend.config(width =549.5, height = 42)

		self.framechat=Frame()
		self.framechat.place (x=270, y=55)
		self.framechat.config(bg="#F2F2F2")	
		self.framechat.config(width= 629.5, height=410.5)

		self.framebarra=Frame()
		self.framebarra.place(x=270, y=0)
		self.framebarra.config(bg="#F2F2F2")
		self.framebarra.config(width=629.5, height=55)

		self.frameconver=Frame()
		self.frameconver.place(x= 0,y = 0)
		self.frameconver.config(bg="#EE8944")
		self.frameconver.config(width =220, height = 55)

		self.ima = PhotoImage(file="src/Genera.png") 
		self.ima2= PhotoImage(file="src/flash1.png")
		self.ima3= 	PhotoImage (file = "src/send.png")
		self.ima4= PhotoImage(file= "src/config.png")

		myscrollbar=Scrollbar(self.raiz,orient=VERTICAL)
		myscrollbar.pack(fill=Y, side=RIGHT, expand=FALSE)

		Hel = font.Font(family="Arial", size = 13)
		Cur = font.Font(family="Bell MT", size= 18)
		Helvfont = font.Font(family="Helvetica", size = 11)

		Label(self.framechat, text="APPX", font = Cur, fg="black").place(x=20, y=0)
		Label(self.framechat, text="APPX", font = Cur, fg="black").place(x=20, y=40)
		Label(self.framechat, text="APPX", font = Cur, fg="black").place(x=20, y=80)
		Label(self.framechat, text="APPX", font = Cur, fg="black").place(x=20, y=120)
		Label(self.framechat, text="APPX", font = Cur, fg="black").place(x=20, y=160)
		Label(self.framechat, text="APPX", font = Cur, fg="black").place(x=20, y=200)
		Label(self.framechat, text="APPX", font = Cur, fg="black").place(x=20, y=240)
		Label(self.framechat, text="APPX", font = Cur, fg="black").place(x=20, y=280)
		Label(self.framechat, text="APPX", font = Cur, fg="black").place(x=20, y=320)
		Label(self.framechat, text="APPX", font = Cur, fg="black").place(x=20, y=360)

		Label(self.raiz, text="APPX", font = Cur, fg="white", bg="#EE8944").place(x=0, y=10,width =190)
		Label(self.raiz, text="Crear grupo", font = Helvfont, fg="white", bg = "#F2A36E", relief=FLAT).place(x=0,y=490,width =125, height = 19.5)
		Label(self.raiz, text="Ingresar Codigo", font = Helvfont, fg="white", bg = "#F2A36E", relief=FLAT).place(x=145,y=490,width =125, height = 19.5)

		#Modificar
		Button(self.raiz, bg= "#F2F2F2",cursor ="hand2", text="Editar", relief=FLAT, activebackground="#F2F2F2", command=self.raiz.deiconify).place(x=800,y=0,width =50, height=25)
		#Modificar
		Button(self.raiz, bg= "#F2F2F2",cursor ="hand2", text="EGrupo", fg="#cc0000", relief=FLAT, activebackground="#F2F2F2", command=self.Salir).place(x=850,y=0,width =50, height=25)
		
		self.NombreGrupo = Button(self.raiz,bg= "#F2F2F2",cursor ="hand2", text="Nombre del grupo ", relief=FLAT, activebackground="#F2F2F2")
		# self.NombreGrupo.bind("<Buttton-1>",self.getId)
		self.NombreGrupo.place(x=290,y=10,width =220, height = 38)
		
		self.ObtenerCodigo = Button(self.raiz,bg= "#F2F2F2",cursor ="hand2", text="Código ", relief=FLAT, activebackground="#F2F2F2")
		self.ObtenerCodigo.place(x=660,y=10,width =120, height = 38)

		self.b1 = Button(self.raiz,bg= "#F4B183",cursor ="hand2",relief=FLAT, activebackground="#F4B183")
		self.b1.bind("<Button-1>",self.print1)
		self.b1.place(x=0,y=54.5,width =270, height = 55)

		self.b2 =Button(self.raiz,bg= "#F2A36E",cursor ="hand2", relief=FLAT, activebackground="#F2A36E")
		self.b2.bind("<Button-1>",self.print2)
		self.b2.place(x=0,y=109,width =270, height = 55)
		
		self.b3 = Button(self.raiz,bg= "#F4B183",cursor ="hand2", relief=FLAT, activebackground="#F4B183")
		self.b3.bind("<Button-1>",self.print3)
		self.b3.place(x=0,y=163.5,width =270, height = 55)
		
		self.b4=Button(self.raiz,bg= "#F2A36E",cursor ="hand2", relief=FLAT, activebackground="#F2A36E")
		self.b4.bind("<Button-1>",self.print4)
		self.b4.place(x=0,y=218,width =270, height = 55)
		
		self.b5=Button(self.raiz,bg= "#F4B183",cursor ="hand2", relief=FLAT, activebackground="#F4B183")
		self.b5.bind("<Button-1>",self.print5)
		self.b5.place(x=0,y=272.5,width =270, height = 55)
		
		self.b6=Button(self.raiz,bg= "#F2A36E",cursor ="hand2", relief=FLAT, activebackground="#F2A36E")
		self.b6.bind("<Button-1>",self.print6)
		self.b6.place(x=0,y=327,width =270, height = 55)
		
		self.b7=Button(self.raiz,bg= "#F4B183",cursor ="hand2", relief=FLAT, activebackground="#F4B183")
		self.b7.bind("<Button-1>", self.print7)
		self.b7.place(x=0,y=381.5,width =270, height = 55)

		Button(self.raiz,bg= "#F2A36E",cursor ="hand2", relief=FLAT, image =self.ima2, activebackground="#F2A36E").place(x=145,y=447,width =125, height = 32)
		cgrupo = Button(self.raiz,bg= "#F2A36E",cursor ="hand2", relief=FLAT, image =self.ima, activebackground="#F2A36E")
		cgrupo.bind("<Button-1>",self.creargrupo)
		cgrupo.place(x=0,y=447,width =125, height = 32)
		Button(self.raiz,bg= "#F4B183",cursor ="hand2", relief=FLAT, image = self.ima3, activebackground="#F4B183").place(x=820,y=466,width =80, height = 42)
		OpenConfig = Button (self.raiz, bg= "#EE8944", cursor = "hand2", relief= FLAT, image= self.ima4,activebackground= "#EE8944")
		OpenConfig.bind("<Button-1>",self.InterHija)
		OpenConfig.place(x=216,y=0, height= 55, width=53.5)
		#config.bind("<Button-1>",self.Config)
		#config.place(x=216,y=0, height= 55, width=53.5)

		Entry(self.raiz,bg= "#F2F2F2", font = Hel, relief=FLAT).place(x=310,y=470,width =490, height = 37)
		
	

		self.sett()

		self.raiz.mainloop()

	def InterHija(self,event):
		InterH = InterfazHija(self.raiz,self.usuario,self.control)
		InterH.Iniciar()
#bLOQUE DE CODIGO
#NO MOVER#
	def sett(self):
		self.Usu = MisGrupos(self.usuario)
		self.grupos = self.Usu.ObtenerGrupos()
		print("Mis grupos son:", self.grupos)
		self.setead = setGrupos(self,self.grupos)
		self.botones= [self.b1, self.b2, self.b3, self.b4, self.b5, self.b6, self.b7]
		contador = -1
		for x in self.setead.setear():
			contador += 1
			print("Ok!!",contador)
			self.botones[contador].config(text=x[1],fg="#fff")

	def Config(self,event):

		self.raiz.destroy()
	
	def creargrupo(self,event):
		CrGr = CrearGrupo(self.raiz,self.usuario,self)
		CrGr.Iniciar()

	def setName(self,id):
		try:
			x= (self.grupos[int(self.botonClick.get())])
			self.Ngrupo.set(x[1])
			self.ObtenerCodigo.config(text ="Código:  "+self.Usu.ObtenerCod(x[0])[0] )
			self.NombreGrupo.config(text=self.Ngrupo.get())

		except:
			print("Error")
			

	def print1 (self,event): 
		self.botonClick.set("0")
		self.setName(self.botonClick.get())

	def print2 (self,event):
		self.botonClick.set("1")
		self.setName(self.botonClick.get())
	def print3 (self,event):
		self.botonClick.set("2")
		self.setName(self.botonClick.get())
	def print4 (self,event):
		self.botonClick.set("3")
		self.setName(self.botonClick.get())
	def print5 (self,event):
		self.botonClick.set("4")
		self.setName(self.botonClick.get())
	def print6 (self,event):
		self.botonClick.set("5")
		self.setName(self.botonClick.get())
	def print7 (self,event):
		self.botonClick.set("6")
		self.setName(self.botonClick.get())