from tkinter import *
from tkinter import font

class Generar ():

    def __init__(self):
        
        self.gc = Tk ()
        self.gc.resizable(0,0)
        self.gc.overrideredirect(True)
        self.gc.geometry("400x200+482+250")

        self.f = Frame()
        self.f.place(x=0,y=0)
        self.f.config(bg ="#F4B183")
        self.f.config(width= "400",height="200")

        self.texto = StringVar()
        self.texto.set("#####")
        
        Helvfont = font.Font(family="Helvetica", size = 20)
        Hel = font.Font(family="Arial" ,size=15)

        Label(self.gc, text ="Your code is:", font= Helvfont,bg="#F4B183",fg="#FFF5EE").place(x=120, y=30)
        Entry (self.gc, border= 0, relief= FLAT, font = Hel, state=DISABLED, textvariable = self.texto).place(x= 120, y=90, width=180, height=30)
        Button (self.gc, text="Copy", font = Helvfont,border= 0, fg="#F2A36E",relief = FLAT, cursor = "hand2").place(x=148,y = 140, width=120, height=35)

        self.gc.mainloop()

g = Generar ()