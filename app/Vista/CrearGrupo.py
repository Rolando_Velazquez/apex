from tkinter import *
from tkinter import font
from app.Controlador.controlchat.CrearGrupos import CrearGrupos

class CrearGrupo():

    def __init__(self,vista,usuario,vchat):
        self.vChat = vchat
        self.vista= vista
        self.usuario = usuario
        self.gc = Toplevel(self.vista,bg ="#F4B183")
        self.gc.resizable(0,0)
        # self.gc.overrideredirect(False)
        self.gc.geometry("400x200+482+250")
        self.gc.transient(master=self.vista)
        self.NombreGrupo = StringVar()
        # self.f = Frame()
        # self.f.place(x=0,y=0)
        # self.f.config(bg ="#F4B183")
        # self.f.config(width= "400",height="200")
    def Iniciar(self):
        print(self.usuario)
        Helvfont = font.Font(family="Helvetica", size = 20)
        Hel = font.Font(family="Arial" ,size=13)

        Label(self.gc, text ="Crear Grupo", font= Helvfont,bg="#F4B183",fg="black").place(x=135, y=20)
        Label(self.gc, text ="Nombre del Grupo: ", font= Hel,bg="#F4B183",fg="black").place(x=120, y=70)

        Entry (self.gc, border= 0, relief= FLAT, textvariable = self.NombreGrupo,font = Hel).place(x= 120, y=100, width=180, height=30)
        B1 = Button (self.gc, text="OK", font = Helvfont, border= 0, fg="black",relief = FLAT, cursor = "hand2", bg="#EE8944")
        B1.bind("<Button-1>",self.crear)
        B1.place(x=148,y = 140, width=120, height=35)
    
    def crear(self,event):
        CG = CrearGrupos(self.NombreGrupo.get(),self.usuario,self.vChat)
        CG.CrearGrupo()
        self.gc.destroy()

