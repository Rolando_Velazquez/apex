from app.Vista.Iniciar import InterfazIniciar
from app.Vista.Index import InterfazIndex
from app.Vista.Registrar import InterfazRegistro
from app.Vista.Chat import Chat
from app.Vista.Registrado import Register
from app.Vista.Olvido import Olvidar
from app.Vista.Configuracion import configurar
# from app.Vista.CrearGrupo import CrearGrupo


class ListaControlAcceso:
  def __init__(self,accion,acceso,usuario):
    self.usuari = usuario
    self.accion = accion
    self.controlo = acceso

    if self.accion == "Index":

      I1 = InterfazIndex()
      I1.Iniciar(self.controlo)

    elif self.accion == "Ingresar":
      I2 = InterfazIniciar()
      I2.Iniciar(self.controlo)
  
    elif self.accion == "Registro":
      I3 = InterfazRegistro()
      I3.Iniciar(self.controlo)
    
    elif self.accion == "Chat":
      I4 = Chat()
      I4.Iniciar(self.controlo,usuario)
    
    elif self.accion == "Registrado":
      I5 = Register()
      I5.Iniciar(self.controlo,usuario)
      
    elif self.accion == "Olvidar":
      I6 = Olvidar()
      I6.Iniciar(self.controlo)

    # elif self.accion == "Configuracion":
    #   I7= configurar(self.usuari)
    #   I7.Iniciar(self.controlo)